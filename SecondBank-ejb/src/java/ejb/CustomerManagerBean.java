/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import appHelper.CustomerState;
import java.util.ArrayList;
import javax.ejb.Remote;
import javax.ejb.Remove;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author liyilin
 */
@Stateless
public class CustomerManagerBean implements CustomerManagerRemote {

    @PersistenceContext
    private EntityManager em;

    public CustomerManagerBean(){}

    public void createCustomer(String name, String address,
            String telNumber) {
        CustomerEntity c = new CustomerEntity();
        c.create(name, address, telNumber);
        em.persist(c);
    }

    public List<CustomerState> getCustomers() {
        Query q = em.createQuery("SELECT c FROM Customer c");
        List stateList = new ArrayList();
        for (Object o : q.getResultList()) {
            CustomerEntity c = (CustomerEntity) o;
            CustomerState custState = new CustomerState(c.getId(), c.getName(), c.getAddress(), c.getTelNumber());
            stateList.add(custState);
        }
        return stateList;
    }

    @Remove
    public void remove() {
        System.out.println("CustomerManagerBean:remove()");
    }

}
