/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

/**
 *
 * @author liyilin
 */
public class CustomerEntityCallbacks {

    @PrePersist
    public void prePersist(CustomerEntity c) {
        System.out.println("CustomerEntityCallbacks: prePersist " + c);
    }

    @PostPersist
    public void postPersist(CustomerEntity c) {
        System.out.println("CustomerEntityCallbacks: postPersist " + c);
    }

    @PreRemove
    public void preRemove(CustomerEntity c) {
        System.out.println("CustomerEntityCallbacks: preRemove " + c);
    }

    @PostRemove
    public void postRemove(CustomerEntity c) {
        System.out.println("CustomerEntityCallbacks: postRemove " + c);
    }

    @PreUpdate
    public void preUpdate(CustomerEntity c) {
        System.out.println("CustomerEntityCallbacks: preUpdate " + c);
    }

    @PostUpdate
    public void postUpdate(CustomerEntity c) {
        System.out.println("CustomerEntityCallbacks: postUpdate " + c);
    }

    @PostLoad
    public void postLoad(CustomerEntity c) {
        System.out.println("CustomerEntityCallbacks: postLoad " + c);
    }
}
