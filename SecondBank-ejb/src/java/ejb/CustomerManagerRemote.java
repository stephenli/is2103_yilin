/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import javax.ejb.Remote;
import java.util.List;
/**
 *
 * @author liyilin
 */
@Remote
public interface CustomerManagerRemote {
    public void createCustomer(String name, String address, String telNumber);
    public List getCustomers();
    public void remove();
}
