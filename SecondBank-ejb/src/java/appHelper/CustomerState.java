/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package appHelper;

import java.io.Serializable;

/**
 *
 * @author liyilin
 */
public class CustomerState implements Serializable {

    private Long id;
    private String name;
    private String address;
    private String telNumber;

    public CustomerState(Long id, String name, String address, String telNumber) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.telNumber = telNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }
}
