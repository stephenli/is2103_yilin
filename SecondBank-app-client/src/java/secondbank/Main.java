/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package secondbank;

import appHelper.CustomerState;
import ejb.CustomerManagerRemote;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.ejb.EJB;

/**
 *
 * @author liyilin
 */
public class Main {

    @EJB
    private static CustomerManagerRemote cm;

    public Main() {
    }

    public static void main(String[] args) {
        Main client = new Main();
        client.doSecondBank(args);
        cm.remove();
    }

    public void doSecondBank(String[] args) {
        try {
            String choice = "";
            while (!choice.equals("0")) {
                System.out.println("\n\n\t\tWelcome to SecondBank");
                System.out.println("1a. Add a Customer");
                System.out.println("1b. List Customers");
                System.out.println();
                System.out.println("0. Exit");

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("\nEnter choice: ");
                choice = br.readLine();

                if (choice.equals("1a")) {
                    displayAddCustomer();
                } else if (choice.equals("1b")) {
                    displayAddCustomer();
                } else if (choice.equals("0")) {
                    return;
                } else {
                    System.out.println("Invalid Choice");
                }
            }

        } catch (Exception ex) {
            System.err.println("Caught an unexpected exception!");
            ex.printStackTrace();
        }
    }

    private void displayAddCustomer() {
        Long customerId;
        String name;
        String address;
        String telNumber;

        try {
            System.out.println("\n\n\t\tAdd a Customer");
            name = getString("name", null);
            address = getString("address", null);
            telNumber = getString("telNumber", null);
            cm.createCustomer(name, address, telNumber);
            System.out.println("\nCustomer added successfully.\n");
        } catch (Exception ex) {
            System.out.println("\nFailed to create customer because " + ex.getMessage() + "\n");
        }
    }

    private void displayListCustomer() {
        System.out.println("\n\n\t\tList Customers");
        for (Object o : cm.getCustomers()) {
            CustomerState cs = (CustomerState) o;
            System.out.println("Id         = " + cs.getId());
            System.out.println("Nmae       = " + cs.getName());
            System.out.println("Address    = " + cs.getAddress());
            System.out.println("Tel Number = " + cs.getTelNumber());
            System.out.println();
        }
    }

    public String getString(String attrName, String oldValue) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String stringValue = null;
        try {
            while (true) {
                System.out.print("Enter " + attrName + (oldValue == null ? "" : "(" + oldValue + ")") + " : ");
                stringValue = br.readLine();
                if (stringValue.length() != 0) {
                    break;
                } else if (stringValue.length() == 0 && stringValue != null) {
                    stringValue = oldValue;
                    break;
                }
                System.out.println("Invalid "+ attrName+"...");
            }
        } catch (Exception ex) {
            System.out.println("\nSystem Error Message: "+ ex.getMessage()+"\n");
        }
        return stringValue.trim();

    }
}
